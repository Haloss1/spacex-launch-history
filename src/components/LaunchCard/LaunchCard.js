import React from "react";
import moment from "moment";
import { Card, Image, Title, Subtitle, Content, Date } from "./Styles";

const LaunchCard = ({ url, patchImg, launchName, launchDate, coreStatus }) => {
  return (
    <Card href={url} rel="noreferrer" target="_blank">
      <Image src={patchImg} alt={launchName} />
      <div>
        <Title>{launchName}</Title>
        <br />
        <Subtitle>Core landing success: </Subtitle>
        <Content>{coreStatus}</Content>
        <br />
        <Subtitle>Launch date: </Subtitle>
        <Date>{moment(launchDate).format("MMMM Do YYYY, h:mm:ss a")}</Date>
      </div>
    </Card>
  );
};

export default LaunchCard;
